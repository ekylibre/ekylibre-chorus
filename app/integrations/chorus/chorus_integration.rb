module Chorus
  mattr_reader :default_options do
    {
      globals: {
        strip_namespaces: true,
        convert_response_tags_to: ->(tag) { tag.snakecase.to_sym },
        raise_errors: true
      },
      locals: {
        advanced_typecasting: true
      }
    }
  end

  class ServiceError < StandardError; end

  class ChorusIntegration < ActionIntegration::Base

    authenticate_with :check do
      parameter :api_key
    end
    
    def check(integration = nil)
      integration = fetch integration
      get_json("https://api.aife.economie.gouv.fr/ppf/e-invoicing/v1.0.0/healthcheck") do |r|
        r.success do
          puts 'check success'.inspect.green
        end
      end
    end

  end
end