=begin
#PPF e-invoicing

#### Avertissements - La publication des swaggers permet d'exposer de manière précise les interactions qui seront possibles à l'aide des API du PPF. - Il ne s'agit pas d'une version de production. Ces swaggers pourront être modifiés ultérieurement (méthodes, endpoints). - L'utilisation de ces swaggers sera possible dés lors qu'une application sera publiée sur PISTE, ce qui n'est pas le cas ce jour. --- --- ### L'API facture permet :  >- de faire des recherches multicritères sur les factures intégrées dans le PPF  >- de déposer une facture à partir de ses données  >- de visualiser le lisible d'une facture (format PDF ou Factur-X)  >- de récupérer les données de la facture et ses pièces-jointes  >- de rajouter des pièces-jointes à une facture  >- de rajouter un cycle de vie à une facture à partir de ses données  >- de récupérer l'historique des statuts d'une facture  Les ressources de l'API sont :  >- **/facture** : avec les méthodes de recherche, création, consultation et mise à jour d'une facture   Exemple de cinématique d'appel pour créer une sollicitation:  >- `POST /facture` : *créer une facture dans le PPF à partir de ses données*   >- `GET /facture/recherche` : *faire des recherches avancées de facture à partir de critères personnalisables* 

OpenAPI spec version: Incrément 2 - Specs Ext 2.2
Contact: api@contact.name
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.46
=end

module SwaggerClient
  class ApiError < StandardError
    attr_reader :code, :response_headers, :response_body

    # Usage examples:
    #   ApiError.new
    #   ApiError.new("message")
    #   ApiError.new(:code => 500, :response_headers => {}, :response_body => "")
    #   ApiError.new(:code => 404, :message => "Not Found")
    def initialize(arg = nil)
      if arg.is_a? Hash
        if arg.key?(:message) || arg.key?('message')
          super(arg[:message] || arg['message'])
        else
          super arg
        end

        arg.each do |k, v|
          instance_variable_set "@#{k}", v
        end
      else
        super arg
      end
    end

    # Override to_s to display a friendly error message
    def to_s
      message
    end

    def message
      if @message.nil?
        msg = "Error message: the server returns an error"
      else
        msg = @message
      end

      msg += "\nHTTP status code: #{code}" if code
      msg += "\nResponse headers: #{response_headers}" if response_headers
      msg += "\nResponse body: #{response_body}" if response_body

      msg
    end

  end
end
