module EkylibreChorus
  class Engine < ::Rails::Engine
    initializer 'ekylibre-chorus.assets.precompile' do |app|
      app.config.assets.precompile += %w[integrations/chorus.png]
    end

    initializer :ekylibre_chorus_extend_navigation do |_app|
      EkylibreChorus::ExtNavigation.add_navigation_xml_to_existing_tree
    end

    initializer :ekylibre_chorus_restfully_manageable do |app|
      app.config.x.restfully_manageable.view_paths << EkylibreChorus::Engine.root.join('app', 'views')
    end

    initializer 'ekylibre-chorus.i18n' do |app|
      app.config.i18n.load_path += Dir[EkylibreChorus::Engine.root.join('config', 'locales', '**', '*.yml')]
    end

  end
end
