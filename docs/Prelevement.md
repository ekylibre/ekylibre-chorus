# SwaggerClient::Prelevement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifiant_reference_mandat** | **String** | Identifiant de référence de mandat | [optional] 
**identifiant_bancaire_creancier** | **String** | Identifiant bancaire du créancier | [optional] 
**identifiant_compte_debite** | **String** | Identifiant de compte débité (IBAN) | [optional] 

