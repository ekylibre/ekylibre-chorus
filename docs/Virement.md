# SwaggerClient::Virement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifiant_compte_paiement** | **String** | Identifiant de compte de paiement | 
**nom_compte_paiement** | **String** | Nom de compte de paiement | [optional] 
**identifiant_prestataire_paiement** | **String** | Identifiant de prestataire de services de paiement | [optional] 

