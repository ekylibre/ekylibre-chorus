# SwaggerClient::TotauxDuDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**somme_montant_net_ligne** | **Float** | Somme des montants nets des lignes de facture | 
**somme_remise_document** | **Float** | Somme des remises au niveau du document | [optional] 
**somme_charge_document** | **Float** | Somme des charges ou frais au niveau du document | [optional] 
**montant_total_facture_hors_tva** | **Float** | Montant total de la facture hors TVA | 
**montant_total_tva_facture** | **Float** | Montant total de TVA de la facture | [optional] 
**montant_total_tva_devise_facture** | **Float** | Montant total de TVA de la facture exprimée (devise de comptabilisation) | [optional] 
**montant_total_facture_avec_tva** | **Float** | Montant total de la Facture, avec la TVA. | 
**montant_paye** | **Float** | Montant payé | [optional] 
**montant_arrondi** | **Float** | Montant arrondi | [optional] 
**montant_a_payer** | **Float** | Montant à payer | 

