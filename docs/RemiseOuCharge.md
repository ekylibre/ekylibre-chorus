# SwaggerClient::RemiseOuCharge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**montant** | **Float** | Montant de la remise ou charge | 
**assiette** | **Float** | Assiette de la remise ou charge | [optional] 
**pourcentage** | **Float** | Pourcentage de remise ou charge | [optional] 
**code_type_tva** | **String** | Code de type de TVA de la remise ou charge | 
**taux_tva** | **Float** | Taux de TVA de la remise ou charge | [optional] 
**motif** | **String** | Motif de la remise ou charge | [optional] 
**code_motif** | **String** | Code de motif de la remise ou charge | [optional] 

