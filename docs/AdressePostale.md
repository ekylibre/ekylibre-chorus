# SwaggerClient::AdressePostale

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**adresse1** | **String** | Adresse - Ligne 1 | [optional] 
**adresse2** | **String** | Adresse - Ligne 2 | [optional] 
**adresse3** | **String** | Adresse - Ligne 3 | [optional] 
**localite** | **String** | Localité | [optional] 
**cp** | **String** | Code postal | [optional] 
**pays** | **String** | Subdivision du pays | [optional] 
**code_pays** | **String** | Code de pays | 

