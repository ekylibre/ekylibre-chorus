# SwaggerClient::NoteDeFacture

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code_sujet_note** | **String** | Code du sujet de la note de facture | [optional] 
**note_facture** | **String** | Note de facture | 

