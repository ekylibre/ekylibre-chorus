# SwaggerClient::HealthcheckApi

All URIs are relative to *https://{environment}aife.economie.gouv.fr/ppf/e-invoicing/v1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_health**](HealthcheckApi.md#get_health) | **GET** /healthcheck | Consultation statut e-invoicing

# **get_health**
> get_health

Consultation statut e-invoicing

Consultation des détails du statut e-invoicing 

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
end

api_instance = SwaggerClient::HealthcheckApi.new

begin
  #Consultation statut e-invoicing
  api_instance.get_health
rescue SwaggerClient::ApiError => e
  puts "Exception when calling HealthcheckApi->get_health: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

nil (empty response body)

### Authorization

[bearer_auth](../README.md#bearer_auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined



