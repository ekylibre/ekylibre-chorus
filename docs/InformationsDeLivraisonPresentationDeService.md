# SwaggerClient::InformationsDeLivraisonPresentationDeService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lieu_livraison** | **String** | Livré à | [optional] 
**identifiant_etablissement_livraison** | **String** | Identifiant de l&#x27;établissement de livraison | [optional] 
**identifiant_schema_livraison** | **String** | Identifiant du schéma | [optional] 
**date_livraison_prestation** | **Date** | Date effective de livraison / fin d&#x27;exécution de la prestation | [optional] 

