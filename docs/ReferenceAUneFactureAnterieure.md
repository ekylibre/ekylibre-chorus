# SwaggerClient::ReferenceAUneFactureAnterieure

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference_facture_anterieure** | **String** | Référence à une facture antérieure | 
**date_emission_facture_anterieure** | **Date** | Date d&#x27;émission de facture antérieure | [optional] 

