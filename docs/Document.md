# SwaggerClient::Document

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference_document_justificatif** | **String** | Référence de document justificatif | [optional] 
**date_depot** | **Date** | Date de dépôt du document | [optional] 
**description** | **String** | Description de document justificatif | [optional] 
**emplacement** | **String** | Emplacement de document externe | [optional] 
**piece** | **String** | Document joint encodé en base 64 | [optional] 
**code_mime** | **String** | Code MIME du document joint Codes Mime autorisés :   - application/pdf   - image/png   - image/jpeg   - text/csv   - application/vnd.openxmlformats-officedocument.spreadsheetml.sheet   - application/vnd.oasis.opendocument.spreadsheet  | [optional] 
**nom_fichier** | **String** | Designation fichier | [optional] 

