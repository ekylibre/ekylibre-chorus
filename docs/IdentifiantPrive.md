# SwaggerClient::IdentifiantPrive

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifiant** | **String** | Identifiant privée de l&#x27;acteur | [optional] 
**identifiant_schema** | **String** | Identifiant du schéma | 

