# SwaggerClient::FactureApi

All URIs are relative to *https://{environment}aife.economie.gouv.fr/ppf/e-invoicing/v1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_facture_by_id**](FactureApi.md#get_facture_by_id) | **GET** /facture/{id} | Consultation des données d&#x27;une facture
[**get_facture_historique_statut**](FactureApi.md#get_facture_historique_statut) | **GET** /facture/{id}/historique-statuts | Consultation de l&#x27;historique des statuts de la facture
[**get_facture_telechargement_by_id**](FactureApi.md#get_facture_telechargement_by_id) | **GET** /facture/{id}/telechargement | Téléchargement d&#x27;une facture
[**get_pj_facture**](FactureApi.md#get_pj_facture) | **GET** /facture/{id}/piece-jointe | Consultation des métadonnées des pièces jointes d&#x27;une facture
[**get_pj_factureby_id**](FactureApi.md#get_pj_factureby_id) | **GET** /facture/{id}/piece-jointe/{idpj} | Télécharge une pièce-jointe dans son format d&#x27;origine
[**post_facture**](FactureApi.md#post_facture) | **POST** /facture | Création d&#x27;une facture
[**post_facture_recherche**](FactureApi.md#post_facture_recherche) | **POST** /facture/recherche | Recherche de facture
[**post_facture_statut**](FactureApi.md#post_facture_statut) | **POST** /facture/{id}/statut | Modification du statut de facture

# **get_facture_by_id**
> GetFactureById200Response get_facture_by_id(id)

Consultation des données d'une facture

Retourne les métadonnées et données d'une facture. Inclut le statut courant de la facture. 

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
end

api_instance = SwaggerClient::FactureApi.new
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # String | 


begin
  #Consultation des données d'une facture
  result = api_instance.get_facture_by_id(id)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling FactureApi->get_facture_by_id: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)|  | 

### Return type

[**GetFactureById200Response**](GetFactureById200Response.md)

### Authorization

[bearer_auth](../README.md#bearer_auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_facture_historique_statut**
> Array&lt;Statut&gt; get_facture_historique_statut(id)

Consultation de l'historique des statuts de la facture

Renvoi le statut courant d'une facture ou son historique  à partir d'une date et/ou jusqu'à une date donnée. Si les paramètres dateDebut et dateFin ne sont pas spécifiés, alors l'intégralité de l'historique est retournée. 

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
end

api_instance = SwaggerClient::FactureApi.new
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # String | Identifiant technique de la facture en base 


begin
  #Consultation de l'historique des statuts de la facture
  result = api_instance.get_facture_historique_statut(id)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling FactureApi->get_facture_historique_statut: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)| Identifiant technique de la facture en base  | 

### Return type

[**Array&lt;Statut&gt;**](Statut.md)

### Authorization

[bearer_auth](../README.md#bearer_auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_facture_telechargement_by_id**
> String get_facture_telechargement_by_id(id, opts)

Téléchargement d'une facture

Téléchargement d'une facture au format structuré ou lisible à partir de son identifiant 

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
end

api_instance = SwaggerClient::FactureApi.new
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # String | 
opts = { 
  format: 'format_example' # String | Les formats acceptés sont application/cii+xml, application/ubl+xml, application/facturx+xml,             application/pdf, origine. Si ce paramètre n'est pas spécifié, il sera à \"origine\" par défaut. 
}

begin
  #Téléchargement d'une facture
  result = api_instance.get_facture_telechargement_by_id(id, opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling FactureApi->get_facture_telechargement_by_id: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)|  | 
 **format** | **String**| Les formats acceptés sont application/cii+xml, application/ubl+xml, application/facturx+xml,             application/pdf, origine. Si ce paramètre n&#x27;est pas spécifié, il sera à \&quot;origine\&quot; par défaut.  | [optional] 

### Return type

**String**

### Authorization

[bearer_auth](../README.md#bearer_auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/cii+xml, application/ubl+xml, application/facturx+xml, application/pdf



# **get_pj_facture**
> Array&lt;Document1&gt; get_pj_facture(id)

Consultation des métadonnées des pièces jointes d'une facture

Retourne les métadonnées de l'ensemble des pièces-jointes d'une facture. 

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
end

api_instance = SwaggerClient::FactureApi.new
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # String | 


begin
  #Consultation des métadonnées des pièces jointes d'une facture
  result = api_instance.get_pj_facture(id)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling FactureApi->get_pj_facture: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)|  | 

### Return type

[**Array&lt;Document1&gt;**](Document1.md)

### Authorization

[bearer_auth](../README.md#bearer_auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **get_pj_factureby_id**
> String get_pj_factureby_id(id, idpj)

Télécharge une pièce-jointe dans son format d'origine

Téléchargement d'une pièce jointe à partir de son identifiant. 

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
end

api_instance = SwaggerClient::FactureApi.new
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # String | 
idpj = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # String | 


begin
  #Télécharge une pièce-jointe dans son format d'origine
  result = api_instance.get_pj_factureby_id(id, idpj)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling FactureApi->get_pj_factureby_id: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)|  | 
 **idpj** | [**String**](.md)|  | 

### Return type

**String**

### Authorization

[bearer_auth](../README.md#bearer_auth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pdf, image/png, image/jpeg, image/csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.oasis.opendocument.spreadsheet



# **post_facture**
> PostFactureReponse201 post_facture(body)

Création d'une facture

Création d'une facture à partir de ses données au format JSON et de ses pièces jointes  optionnelles ou de ses données au format structuré (UBL, CII, Factur-X)  Retourne l'identifiant de la facture créée 

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
end

api_instance = SwaggerClient::FactureApi.new
body = SwaggerClient::Facture.new # Facture | Données de la facture


begin
  #Création d'une facture
  result = api_instance.post_facture(body)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling FactureApi->post_facture: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Facture**](Facture.md)| Données de la facture | 

### Return type

[**PostFactureReponse201**](PostFactureReponse201.md)

### Authorization

[bearer_auth](../README.md#bearer_auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **post_facture_recherche**
> PostFactureRechercheReponse200 post_facture_recherche(body)

Recherche de facture

Recherche multicritère de factures  Retourne les identifiants de factures, les données essentielles de ces factures et statuts. 

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
end

api_instance = SwaggerClient::FactureApi.new
body = SwaggerClient::PostFactureRechercheRequete.new # PostFactureRechercheRequete | 


begin
  #Recherche de facture
  result = api_instance.post_facture_recherche(body)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling FactureApi->post_facture_recherche: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PostFactureRechercheRequete**](PostFactureRechercheRequete.md)|  | 

### Return type

[**PostFactureRechercheReponse200**](PostFactureRechercheReponse200.md)

### Authorization

[bearer_auth](../README.md#bearer_auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



# **post_facture_statut**
> post_facture_statut(bodyid)

Modification du statut de facture

Provoque le changement de statut de la facture. Une pièce-jointe peut être ajoutée lors du passage à certains statuts (complétée) 

### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
end

api_instance = SwaggerClient::FactureApi.new
body = SwaggerClient::PostFactureStatutRequete.new # PostFactureStatutRequete | Données de la facture
id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # String | Identifiant technique de la facture en base 


begin
  #Modification du statut de facture
  api_instance.post_facture_statut(bodyid)
rescue SwaggerClient::ApiError => e
  puts "Exception when calling FactureApi->post_facture_statut: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PostFactureStatutRequete**](PostFactureStatutRequete.md)| Données de la facture | 
 **id** | [**String**](.md)| Identifiant technique de la facture en base  | 

### Return type

nil (empty response body)

### Authorization

[bearer_auth](../README.md#bearer_auth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined



