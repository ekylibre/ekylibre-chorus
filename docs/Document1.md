# SwaggerClient::Document1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**code_mime** | **String** | Code MIME du document joint Codes Mime autorisés :   - application/pdf   - image/png   - image/jpeg   - text/csv   - application/vnd.openxmlformats-officedocument.spreadsheetml.sheet   - application/vnd.oasis.opendocument.spreadsheet  | [optional] 
**nom_fichier** | **String** | Designation fichier | [optional] 
**reference_document_justificatif** | **String** | Référence de document justificatif | [optional] 
**date_depot** | **Date** | Date de dépôt du document | [optional] 
**description** | **String** | Description de document justificatif | [optional] 
**emplacement** | **String** | Emplacement de document externe | [optional] 

