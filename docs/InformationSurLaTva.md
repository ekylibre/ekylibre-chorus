# SwaggerClient::InformationSurLaTva

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code_type_tva** | **String** | Code de type de TVA de l&#x27;article facturé | 
**taux_tva** | **Float** | Taux de TVA de l&#x27;article facturé | [optional] 

