# SwaggerClient::DetailDuPrix

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prix_net** | **Float** | Prix net de l&#x27;article | 
**rabais_prix** | **Float** | Rabais sur le prix de l&#x27;article | [optional] 
**prix_brut** | **Float** | Prix brut de l&#x27;article | [optional] 
**quantite_base_prix** | **Float** | Quantité de base du prix de l&#x27;article | [optional] 
**code_quantite_prix** | **String** | Code de l&#x27;unité de mesure de la quantité de base du prix de l&#x27;article | [optional] 

