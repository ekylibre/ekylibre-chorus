# SwaggerClient::RepresentantFiscalDuVendeur

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nom** | **String** | Nom du représentant fiscal du vendeur | [optional] 
**identifiant_tva** | **String** | Identifiant à la TVA du représentant fiscal du vendeur | [optional] 
**adresse_postale** | [**AdressePostale**](AdressePostale.md) |  | [optional] 

