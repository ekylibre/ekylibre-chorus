# SwaggerClient::InformationSurArticle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**designation** | **String** | Nom de l&#x27;article | 
**description** | **String** | Description de l&#x27;article | [optional] 
**identifiant_vendeur** | **String** | Identifiant vendeur de l&#x27;article | [optional] 
**identifiant_acheteur** | **String** | Identifiant acheteur de l&#x27;article | [optional] 
**identifiant_standard** | **String** | Identifiant standard de l&#x27;article | [optional] 
**identifiant_schema_standard** | **String** | Identifiant du schéma | 
**identifiant_de_classification** | [**IdentifiantDeClassification**](IdentifiantDeClassification.md) |  | [optional] 

