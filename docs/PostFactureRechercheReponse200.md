# SwaggerClient::PostFactureRechercheReponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**Array&lt;Facture&gt;**](Facture.md) |  | 

