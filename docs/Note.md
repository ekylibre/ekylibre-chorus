# SwaggerClient::Note

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**note** | **String** | Note de ligne de facture | [optional] 

