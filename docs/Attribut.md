# SwaggerClient::Attribut

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nom_attribut** | **String** | Nom d&#x27;attribut d&#x27;article | 
**valeur_attribut** | **String** | Valeur d&#x27;attribut d&#x27;article | 

