# SwaggerClient::GetFactureById200Response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numero_facture** | **String** | Numéro de facture | 
**date_emission** | **Date** | Date d&#x27;émission facture initiale / facture rectificative | 
**code_type** | **String** | Code de type de facture | [optional] 
**code_devise** | **String** | Code de devise de la facture | 
**code_devise_tva** | **String** | Code de devise de comptabilisation de la TVA | [optional] 
**date_exigibilite_tva** | **Date** | Date d&#x27;exigibilité de la taxe sur la valeur ajoutée | [optional] 
**code_date_exigibilite_tva** | **String** | Code de date d&#x27;exigibilité de la taxe sur la valeur ajoutée | [optional] 
**date_echeance** | **Date** | Date d&#x27;échéance | [optional] 
**reference_acheteur** | **String** | Référence de l&#x27;acheteur | [optional] 
**reference_projet** | **String** | Référence de projet | [optional] 
**reference_contrat** | **String** | Référence du contrat | [optional] 
**reference_bon_commande** | **String** | Référence du bon de commande | [optional] 
**numero_ordre_vente** | **String** | Numéro d’ordre de vente | [optional] 
**reference_avis_reception** | **String** | Référence d&#x27;avis de réception | [optional] 
**reference_avis_expedition** | **String** | Référence d&#x27;avis d&#x27;expédition | [optional] 
**reference_appel_offre** | **String** | Référence de l&#x27;appel d&#x27;offres ou du lot | [optional] 
**identifiant_objet_facture** | **String** | Identifiant d&#x27;objet facturé | [optional] 
**identifiant_schema** | **String** | Identifiant du schéma | [optional] 
**reference_comptable_acheteur** | **String** | Référence comptable de l&#x27;acheteur | [optional] 
**condition_paiement** | **String** | Conditions de paiement | [optional] 
**note_de_facture** | [**Array&lt;NoteDeFacture&gt;**](NoteDeFacture.md) |  | [optional] 
**controle_du_processus** | [**ControleDuProcessus**](ControleDuProcessus.md) |  | 
**reference_a_une_facture_anterieure** | [**Array&lt;ReferenceAUneFactureAnterieure&gt;**](ReferenceAUneFactureAnterieure.md) |  | [optional] 
**vendeur** | [**Acteur**](Acteur.md) |  | 
**acheteur** | [**Acteur**](Acteur.md) |  | 
**beneficiaire** | [**Beneficiaire**](Beneficiaire.md) |  | [optional] 
**representant_fiscal_du_vendeur** | [**RepresentantFiscalDuVendeur**](RepresentantFiscalDuVendeur.md) |  | [optional] 
**informations_de_livraison_presentation_de_service** | [**InformationsDeLivraisonPresentationDeService**](InformationsDeLivraisonPresentationDeService.md) |  | [optional] 
**periode_de_facturation** | [**PeriodeDeFacturation**](PeriodeDeFacturation.md) |  | [optional] 
**adresse_de_livraison_realisation_prestation_de_service** | [**AdressePostale**](AdressePostale.md) |  | [optional] 
**instructions_de_paiement** | [**InstructionsDePaiement**](InstructionsDePaiement.md) |  | [optional] 
**remise_au_niveau_du_document** | [**Array&lt;RemiseOuCharge&gt;**](RemiseOuCharge.md) |  | [optional] 
**charges_ou_frais_au_niveau_du_document** | [**Array&lt;RemiseOuCharge&gt;**](RemiseOuCharge.md) |  | [optional] 
**totaux_du_document** | [**TotauxDuDocument**](TotauxDuDocument.md) |  | 
**ventilation_de_la_tva** | [**Array&lt;VentilationDeLaTva&gt;**](VentilationDeLaTva.md) |  | 
**documents_justificatifs_additionnels** | [**Array&lt;Document&gt;**](Document.md) |  | [optional] 
**ligne_de_facture** | [**Array&lt;LigneDeFacture&gt;**](LigneDeFacture.md) |  | 
**statut** | [**Statut**](Statut.md) |  | [optional] 

