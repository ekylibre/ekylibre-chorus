# SwaggerClient::Beneficiaire

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nom** | **String** | Nom du bénéficiaire | 
**identifiant** | **String** | Identifiant du bénéficiaire | [optional] 
**identifiant_schema** | **String** | Identifiant du schéma | [optional] 
**identifiant_legal** | **String** | Identifiant d’enregistrement légal du bénéficiaire | [optional] 
**identifiant_schema_identifiant** | **String** | Identifiant du schéma | [optional] 

