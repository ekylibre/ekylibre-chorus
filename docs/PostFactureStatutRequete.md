# SwaggerClient::PostFactureStatutRequete

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code_statut** | **String** | Le code du statut cible | 
**motif** | **String** | Un motif lié au changement de statut | [optional] 
**piece** | **String** | Document joint encodé en base 64 | [optional] 

