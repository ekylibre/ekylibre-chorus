# SwaggerClient::Contact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**point_contact** | **String** | Point de contact | [optional] 
**telephone** | **String** | Numéro de téléphone du contact | [optional] 
**email** | **String** | Adresse électronique du contact | [optional] 

