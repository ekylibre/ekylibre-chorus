# SwaggerClient::PostFactureRechercheRequete

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**where** | [**Where**](Where.md) |  | [optional] 
**sort** | [**Sort**](Sort.md) |  | [optional] 
**fields** | [**Fields**](Fields.md) |  | [optional] 
**limit** | [**Limit**](Limit.md) |  | [optional] 
**offset** | [**Offset**](Offset.md) |  | [optional] 

