# SwaggerClient::VentilationDeLaTva

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base_imposition_tva** | **Float** | Base d&#x27;imposition du type de TVA | 
**montant_type_tva** | **Float** | Montant de la TVA pour chaque type de TVA | 
**code_type_tva** | **String** | Code de type de TVA | 
**taux_type_tva** | **Float** | Taux de type de TVA | [optional] 
**motif_exoneration_tva** | **String** | Motif d&#x27;exonération de la TVA | [optional] 
**code_motif_exoneration_tva** | **String** | Code de motif d&#x27;exonération de la TVA | [optional] 

