# SwaggerClient::ControleDuProcessus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type_processus** | **String** | Type de processus métier (cadre de facturation) | [optional] 
**type_profil** | **String** | Type de profil (e-invoicing, e-reporting, facture etc..) | 

