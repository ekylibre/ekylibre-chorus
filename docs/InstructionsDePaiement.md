# SwaggerClient::InstructionsDePaiement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code_moyen** | **String** | Code de type de moyen de paiement | [optional] 
**libelle_moyen** | **String** | Libellé du moyen de paiement | [optional] 
**avis** | **String** | Avis de paiement | [optional] 
**virement** | [**Virement**](Virement.md) |  | [optional] 
**informations_concernant_la_carte_de_paiement** | [**InformationsConcernantLaCarteDePaiement**](InformationsConcernantLaCarteDePaiement.md) |  | [optional] 
**prelevement** | [**Prelevement**](Prelevement.md) |  | [optional] 

