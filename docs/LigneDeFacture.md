# SwaggerClient::LigneDeFacture

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifiant** | **String** | Identifiant de ligne de facture | [optional] 
**note** | [**Array&lt;Note&gt;**](Note.md) |  | [optional] 
**identifiant_objet** | **String** | Identifiant d&#x27;objet de ligne de facture | [optional] 
**identifiant_schema** | **String** | Identifiant du schéma | [optional] 
**quantite_facture** | **Float** | Quantité facturée | [optional] 
**code_quantite_facture** | **String** | Code de l&#x27;unité de mesure de la quantité facturée | [optional] 
**montant_net_facture** | **Float** | Montant net de ligne de facture | [optional] 
**reference_bon_commande** | **String** | Référence de ligne de bon de commande référencée | [optional] 
**reference_comptable_acheteur_facture** | **String** | Référence comptable de l&#x27;acheteur de la ligne de facture | [optional] 
**periode_de_facturation** | [**PeriodeDeFacturation**](PeriodeDeFacturation.md) |  | [optional] 
**remise** | [**RemiseOuCharge**](RemiseOuCharge.md) |  | [optional] 
**charge_ou_frais** | [**RemiseOuCharge**](RemiseOuCharge.md) |  | [optional] 
**detail_du_prix** | [**DetailDuPrix**](DetailDuPrix.md) |  | 
**information_sur_la_tva** | [**InformationSurLaTva**](InformationSurLaTva.md) |  | 
**information_sur_article** | [**InformationSurArticle**](InformationSurArticle.md) |  | 

