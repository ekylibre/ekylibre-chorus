# SwaggerClient::Statut

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** | code identifiant le statut | [optional] 
**canal** | **String** | canal de réception du statut (API/Portail/EDI) | [optional] 
**utilisateur_origin** | [**UtilisateurOrigin**](UtilisateurOrigin.md) |  | [optional] 
**motif** | **String** | motif de changement de statut | [optional] 

