# SwaggerClient::IdentifiantDeClassification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifiant_classification** | **String** | Identifiant de la classification de l&#x27;article | [optional] 
**identifiant_schema_classification** | **String** | Identifiant du schéma | 
**identifiant_version_schema** | **String** | Identifiant version du schéma | [optional] 
**attributs** | [**Array&lt;Attribut&gt;**](Attribut.md) |  | [optional] 

