# SwaggerClient::InformationsConcernantLaCarteDePaiement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numero_compte_primaire** | **String** | Identifiant de compte de paiement | [optional] 
**nom_titulaire** | **String** | Nom de compte de paiement | [optional] 

