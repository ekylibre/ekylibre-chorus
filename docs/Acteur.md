# SwaggerClient::Acteur

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**raison_sociale** | **String** | Raison sociale de l&#x27;acteur | 
**appellation_commerciale** | **String** | Appellation commerciale de l&#x27;acteur | [optional] 
**identifiant_prive** | [**Array&lt;IdentifiantPrive&gt;**](IdentifiantPrive.md) |  | [optional] 
**identifiant_siret** | **String** | Identifiant de l&#x27;acteur (SIRET) | [optional] 
**identifiant_schema_siret** | **String** | Identifiant du schéma (SIRET) | 
**code_service** | **String** | Identifiant de l&#x27;acteur (Code routage) | [optional] 
**identifiant_schema_code_service** | **String** | Identifiant du schéma (Code routage) | 
**numero_siren** | **String** | Numéro de SIREN | [optional] 
**identifiant_schema_identifiant** | **String** | Identifiant du schéma | [optional] 
**qualifiant_identifiant_fiscal** | **String** | Qualifiant Identifiant fiscal de l&#x27;acteur | 
**identifiant_tva** | **String** | Identifiant à la TVA  de l&#x27;acteur | [optional] 
**email** | **String** | Adresse électronique de l&#x27;acteur (adresse de facturation) | [optional] 
**identifiant_email** | **String** | Identifiant du schéma de l&#x27;adresse électronique de l&#x27;acteur | 
**forme_juridique** | **String** | Identifiant du schéma de l&#x27;adresse électronique de l&#x27;acteur | [optional] 
**adresse_postale** | [**AdressePostale**](AdressePostale.md) |  | 
**contact** | [**Contact**](Contact.md) |  | [optional] 

