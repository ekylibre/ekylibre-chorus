=begin
#PPF e-invoicing

#### Avertissements - La publication des swaggers permet d'exposer de manière précise les interactions qui seront possibles à l'aide des API du PPF. - Il ne s'agit pas d'une version de production. Ces swaggers pourront être modifiés ultérieurement (méthodes, endpoints). - L'utilisation de ces swaggers sera possible dés lors qu'une application sera publiée sur PISTE, ce qui n'est pas le cas ce jour. --- --- ### L'API facture permet :  >- de faire des recherches multicritères sur les factures intégrées dans le PPF  >- de déposer une facture à partir de ses données  >- de visualiser le lisible d'une facture (format PDF ou Factur-X)  >- de récupérer les données de la facture et ses pièces-jointes  >- de rajouter des pièces-jointes à une facture  >- de rajouter un cycle de vie à une facture à partir de ses données  >- de récupérer l'historique des statuts d'une facture  Les ressources de l'API sont :  >- **/facture** : avec les méthodes de recherche, création, consultation et mise à jour d'une facture   Exemple de cinématique d'appel pour créer une sollicitation:  >- `POST /facture` : *créer une facture dans le PPF à partir de ses données*   >- `GET /facture/recherche` : *faire des recherches avancées de facture à partir de critères personnalisables* 

OpenAPI spec version: Incrément 2 - Specs Ext 2.2
Contact: api@contact.name
Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 3.0.46
=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for SwaggerClient::Fields
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'Fields' do
  before do
    # run before each test
    @instance = SwaggerClient::Fields.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of Fields' do
    it 'should create an instance of Fields' do
      expect(@instance).to be_instance_of(SwaggerClient::Fields)
    end
  end
end
